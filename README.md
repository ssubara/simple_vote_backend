
## Local build and run

```bash
$ npm install && npm run build

```

## Running the docker container

```bash
$ docker build -t polling-service:latest .
$ docker run -it -p 8081:8081 polling-service:latest 

```