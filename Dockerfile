FROM node:lts-alpine

# Create app directory
WORKDIR /usr/src

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

# Building your code for production
RUN npm ci --only=production

# Bundle app source
COPY . .

EXPOSE 8081

CMD ["npm","run","start:prod"]