import { Controller, Post, UseInterceptors, UploadedFile, Res } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { Response } from 'express';
import { extname } from 'path';
import { diskStorage } from 'multer'

@Controller('/votes/api/v1/uploadfile')
export class UploadFileController {

@Post()
@UseInterceptors(
    FileInterceptor('file', {
        storage: diskStorage({
          destination: './log/votes',
          filename:(req, file, callback) => {
            const name = file.originalname.split('.')[0];
            const fileExtName = extname(file.originalname);           
            callback(null, `${name}${fileExtName}`);}
        }),
        limits: {fileSize:2*1024*1024,fieldSize:2*1024*1024}
      }
      )
 )
async uploadFile(@UploadedFile() file, @Res() response:Response) {
    const responseData = {
        "originalname": file.originalname,
        "filename": file.filename,
      };
      response.end("File Uploaded Successfully with Name:"+responseData.filename);
    }

}
