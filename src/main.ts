import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import bodyParser = require('body-parser');
var serveIndex = require('serve-index');
import * as express from 'express';
import path = require('path');

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
            bodyParser: false
  });  
  app.use(express.static(path.join('./log/votes')));
  // app.use('/votes', serveIndex('./log/votes', {'icons': true,'view':'details'}));
  app.enableCors();
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  await app.listen(8081);
}
bootstrap();
