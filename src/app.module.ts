import { Module, HttpModule } from '@nestjs/common';
import { configure } from "log4js";
import { MulterModule } from '@nestjs/platform-express';
import { UploadFileController } from './upload-file/upload-file.controller';
import { VotesController } from './votes/votes.controller';
configure("./config/log4js.json");

@Module({
  imports: [MulterModule.register({
    dest: './log/votes',
  }),HttpModule],
  controllers: [ UploadFileController, VotesController]
})
export class AppModule {}
