import { Controller, Get, Post, Param, Res, HttpStatus, Body, Req } from '@nestjs/common';
import * as rawbody from 'raw-body';
import { Response } from 'express';
import * as myVotes from 'log/votes/poll.json';
import * as fs from 'fs';
import { getLogger } from 'log4js';
const logger = getLogger();
logger.level = "debug";

@Controller('votes')

export class VotesController {
    @Post()
    async index(@Body() data, @Req() req) {
        interface vote {
            poll_id: string;
            question_id: string;
            q_opt: string;
        }
        let allVotes:vote[];
        const fs = require('fs');
        // If is first record in file, file must contain only [] (empty array)
        var global_data = fs.readFileSync('log/votes/poll.json').toString();
        allVotes = JSON.parse(global_data);
        let newVote:vote;
        if (req.readable) {
            // body is ignored by NestJS -> get raw body from request
           
            const raw = await rawbody(req);
            var vote_str = raw.toString().trim();
            newVote = JSON.parse(vote_str);
          } else {
            // body is parsed by NestJS
            var val = JSON.stringify(req.body, null, 2);
            newVote = JSON.parse(val);
          }
        
        allVotes.push(newVote)
        var jsonData = JSON.stringify(allVotes, null, 2);

        fs.writeFile('log/votes/poll.json', jsonData, function(err) {
            console.log('Successfully wrote file')
        });
        return 'Added new vote';
    }
  
    // Return all votes 
    @Get()
    getVotes(@Res() res: Response): any {
        res.status(HttpStatus.OK).json(myVotes);
    }

    // Return votes by ID of poll
    @Get(':id')
    findPollId(@Param('id') id): string{
        
        interface vote {
            poll_id: string;
            question_id: string;
            q_opt: string;
        }

        let votes:vote[];
        var filterVotes:vote[];
        var filterVotes1:vote[];
        var filterVotes2:vote[];
        var filterVotes3:vote[];
        var filterVotes4:vote[];
        const fs = require('fs');

        var global_data = fs.readFileSync('log/votes/poll.json').toString();
        votes = JSON.parse(global_data);
        filterVotes = votes.filter(x => ( x.poll_id == id ) )

        if (filterVotes.length == 0) {
            return 'No poll with this ID';
        }

        function filterForQ (numQ) {
            return filterVotes.filter(x => ( x.question_id == `${numQ}`) )
        }
        
        filterVotes1 = filterForQ (1);
        filterVotes2 = filterForQ (2);
        filterVotes3 = filterForQ (3);
        filterVotes4 = filterForQ (4);

        function procents(numV) {
            return ((numV*100)/filterVotes.length).toFixed(2);
        }

        var procent1 = procents(filterVotes1.length);
        var procent2 = procents(filterVotes2.length);
        var procent3 = procents(filterVotes3.length);
        var procent4 = procents(filterVotes4.length);

        const results = {
            ugcPollId: `${id}`,
            numOfVotes: `${filterVotes.length}`,
            question_1: `${procent1}`,
            question_2: `${procent2}`,
            question_3: `${procent3}`,
            question_4: `${procent4}`,        
        }
        let strRes = JSON.stringify(results);
        return `${strRes}`;
    }
    

}