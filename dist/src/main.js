"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
const bodyParser = require("body-parser");
var serveIndex = require('serve-index');
const express = require("express");
const path = require("path");
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule, {
        bodyParser: false
    });
    app.use(express.static(path.join('./log/votes')));
    app.enableCors();
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    await app.listen(8081);
}
bootstrap();
//# sourceMappingURL=main.js.map