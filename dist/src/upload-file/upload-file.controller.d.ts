import { Response } from 'express';
export declare class UploadFileController {
    uploadFile(file: any, response: Response): Promise<void>;
}
