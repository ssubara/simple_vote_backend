"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const platform_express_1 = require("@nestjs/platform-express");
const path_1 = require("path");
const multer_1 = require("multer");
let UploadFileController = class UploadFileController {
    async uploadFile(file, response) {
        const responseData = {
            "originalname": file.originalname,
            "filename": file.filename,
        };
        response.end("File Uploaded Successfully with Name:" + responseData.filename);
    }
};
__decorate([
    common_1.Post(),
    common_1.UseInterceptors(platform_express_1.FileInterceptor('file', {
        storage: multer_1.diskStorage({
            destination: './log/votes',
            filename: (req, file, callback) => {
                const name = file.originalname.split('.')[0];
                const fileExtName = path_1.extname(file.originalname);
                callback(null, `${name}${fileExtName}`);
            }
        }),
        limits: { fileSize: 2 * 1024 * 1024, fieldSize: 2 * 1024 * 1024 }
    })),
    __param(0, common_1.UploadedFile()), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UploadFileController.prototype, "uploadFile", null);
UploadFileController = __decorate([
    common_1.Controller('/votes/api/v1/uploadfile')
], UploadFileController);
exports.UploadFileController = UploadFileController;
//# sourceMappingURL=upload-file.controller.js.map