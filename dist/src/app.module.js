"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const log4js_1 = require("log4js");
const platform_express_1 = require("@nestjs/platform-express");
const upload_file_controller_1 = require("./upload-file/upload-file.controller");
const votes_controller_1 = require("./votes/votes.controller");
log4js_1.configure("./config/log4js.json");
let AppModule = class AppModule {
};
AppModule = __decorate([
    common_1.Module({
        imports: [platform_express_1.MulterModule.register({
                dest: './log/votes',
            }), common_1.HttpModule],
        controllers: [upload_file_controller_1.UploadFileController, votes_controller_1.VotesController]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map