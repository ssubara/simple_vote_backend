"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const rawbody = require("raw-body");
const myVotes = require("../../log/votes/poll.json");
const log4js_1 = require("log4js");
const logger = log4js_1.getLogger();
logger.level = "debug";
let VotesController = class VotesController {
    async index(data, req) {
        let allVotes;
        const fs = require('fs');
        var global_data = fs.readFileSync('log/votes/poll.json').toString();
        allVotes = JSON.parse(global_data);
        let newVote;
        if (req.readable) {
            const raw = await rawbody(req);
            var vote_str = raw.toString().trim();
            newVote = JSON.parse(vote_str);
        }
        else {
            var val = JSON.stringify(req.body, null, 2);
            newVote = JSON.parse(val);
        }
        allVotes.push(newVote);
        var jsonData = JSON.stringify(allVotes, null, 2);
        fs.writeFile('log/votes/poll.json', jsonData, function (err) {
            console.log('Successfully wrote file');
        });
        return 'Added new vote';
    }
    getVotes(res) {
        res.status(common_1.HttpStatus.OK).json(myVotes);
    }
    findPollId(id) {
        let votes;
        var filterVotes;
        var filterVotes1;
        var filterVotes2;
        var filterVotes3;
        var filterVotes4;
        const fs = require('fs');
        var global_data = fs.readFileSync('log/votes/poll.json').toString();
        votes = JSON.parse(global_data);
        filterVotes = votes.filter(x => (x.poll_id == id));
        if (filterVotes.length == 0) {
            return 'No poll with this ID';
        }
        function filterForQ(numQ) {
            return filterVotes.filter(x => (x.question_id == `${numQ}`));
        }
        filterVotes1 = filterForQ(1);
        filterVotes2 = filterForQ(2);
        filterVotes3 = filterForQ(3);
        filterVotes4 = filterForQ(4);
        function procents(numV) {
            return ((numV * 100) / filterVotes.length).toFixed(2);
        }
        var procent1 = procents(filterVotes1.length);
        var procent2 = procents(filterVotes2.length);
        var procent3 = procents(filterVotes3.length);
        var procent4 = procents(filterVotes4.length);
        const results = {
            ugcPollId: `${id}`,
            numOfVotes: `${filterVotes.length}`,
            question_1: `${procent1}`,
            question_2: `${procent2}`,
            question_3: `${procent3}`,
            question_4: `${procent4}`,
        };
        let strRes = JSON.stringify(results);
        return `${strRes}`;
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Body()), __param(1, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], VotesController.prototype, "index", null);
__decorate([
    common_1.Get(),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Object)
], VotesController.prototype, "getVotes", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", String)
], VotesController.prototype, "findPollId", null);
VotesController = __decorate([
    common_1.Controller('votes')
], VotesController);
exports.VotesController = VotesController;
//# sourceMappingURL=votes.controller.js.map