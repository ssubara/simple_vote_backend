import { Response } from 'express';
export declare class VotesController {
    index(data: any, req: any): Promise<string>;
    getVotes(res: Response): any;
    findPollId(id: any): string;
}
